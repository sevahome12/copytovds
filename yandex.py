from importlib.resources import path
from os.path import getctime
from datetime import datetime as dt
import yadisk
import config
import os
for token in config.YaTokens:
    y = yadisk.YaDisk(token=config.YaTokens[token])
    path0 ='test/'+token
    if not os.path.exists(path0):
        os.mkdir(path0)
    path2 = ''

    def cycle_dir(item, path, path2):
        if(item['type'] != 'dir'):
            filename = item['path'].replace('disk:', path0, 1)
            if os.path.exists(filename) and dt.fromtimestamp(getctime(filename)).strftime('%Y-%m-%d %H:%M:%S') >= str(item['modified'])[:-6]: 
                return
            y.download(item['path'],path +'/' +item['name'])
            return
        if(item['type'] == 'dir'):
            path = path + '/'+item['name']
            path2 = path2 + '/' + item['name']
            if not os.path.exists(path):
                os.mkdir(path)
            for item2 in y.listdir(path2):
                cycle_dir(item2, path, path2)
        
    for item in y.listdir('/'):
        cycle_dir(item, path0, '')

